/*
 * ICT1002 (C Language) Group Project, AY19 Trimester 1.
 *
 * This file contains the definitions and function prototypes for all of
 * features of the ICT1002 chatbot.
 */
 
#ifndef _CHAT1002_H
#define _CHAT1002_H

#include <stdio.h>

/* the maximum number of characters we expect in a line of input (including the terminating null)  */
#define MAX_INPUT    256

/* the maximum number of characters allowed in the name of an intent (including the terminating null)  */
#define MAX_INTENT   32

/* the maximum number of characters allowed in the name of an entity (including the terminating null)  */
#define MAX_ENTITY   64

/* the maximum number of characters allowed in a response (including the terminating null) */
#define MAX_RESPONSE 2048

/* return codes for knowledge_get() and knowledge_put() */
#define KB_OK        0
#define KB_NOTFOUND -1
#define KB_INVALID  -2
#define KB_NOMEM    -3
 
/* functions defined in main.c */
int compare_token(const char *token1, const char *token2);
void prompt_user(char *buf, int n, const char *format, ...);

/* functions defined in chatbot.c */
const char *chatbot_botname();
const char *chatbot_username();
void chatbot_textcol_set_chatbot();
void chatbot_textcol_set_user();
void chatbot_textcol_set_default();
int chatbot_main(int inc, char *inv[], char *response, int n);
int chatbot_update(int inc, char* inv[], char* response, int n);
int chatbot_is_exit(const char *intent);
int chatbot_do_exit(int inc, char *inv[], char *response, int n);
int chatbot_is_load(const char *intent);
int chatbot_do_load(int inc, char *inv[], char *response, int n);
int chatbot_is_question(const char *intent);
int chatbot_do_question(int inc, char *inv[], char *response, int n);
int chatbot_is_arith(const char* intent);
int chatbot_do_arith(int inc, char *inv[], char * response, int n);
void do_arith_1_operand(char *inv[], float n1, char* response, int n);
void do_arith_2_operand(char* inv[], float n1, float n2, char* response, int n);
int checkNum1(char* inv[]);
int checkNum2(char* inv[]);
int checkNum2WithAnd(char* inv[]);
int chatbot_is_clear_screen(const char* intent);
int chatbot_do_clear_screen(int inc, char* inv[], char* response, int n);
int chatbot_is_reset(const char *intent);
int chatbot_do_reset(int inc, char *inv[], char *response, int n, int display_reset_msg);
int chatbot_is_save(const char *intent);
int chatbot_do_save(int inc, char *inv[], char *response, int n);
int chatbot_is_name(const char* intent);
int chatbot_do_name(int inc, char* inv[], char* response, int n);
int chatbot_is_smalltalk(const char *intent);
int chatbot_do_smalltalk(int inc, char *inv[], char *resonse, int n);
int chatbot_record_response(int inc, char* inv[], char* response, int n);
int chatbot_is_change_color(const char* intent);
int chatbot_do_change_color(int inc, char* inv[], char* response, int n);
int chatbot_is_hangman(const char* intent);
int chatbot_do_HangMan(int inc, char* inv[], char* response, int n);
int chatbot_is_randomNo(const char* intent);
int chatbot_do_randomNo(int inc, char* inv[], char* response, int n);
int chatbot_is_team25Info(const char* intent);
int chatbot_do_team25Info(int inc, char* inv[], char* response, int n);
int chatbot_is_help(const char* intent);
int chatbot_do_help(int inc, char* inv[], char* response, int n);
int helper_menu(int inc, char* inv[], char* response, int n);

/* functions defined in knowledge.c */
int knowledge_get(const char *intent, const char *entity, char *response, int n);
int knowledge_put(const char* intent, const char* entity, const char* response);
void knowledge_reset();
int knowledge_read(FILE *f);
void knowledge_write(FILE *f);
int add_knowledge_to_database(void* item);

int checkAlpha(char* input);  //checks if word doesnt contain digits or symbols
int guesscheck(char guess);   //checks if the p2guess matches a word in p1guess
int checkResult();            //checks if the overall word is formed



#endif