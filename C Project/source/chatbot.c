/*
 * ICT1002 (C Language) Group Project, AY19 Trimester 1.
 *
 * This file implements the behaviour of the chatbot. The main entry point to
 * this module is the chatbot_main() function, which identifies the intent
 * using the chatbot_is_*() functions then invokes the matching chatbot_do_*()
 * function to carry out the intent.
 *
 * chatbot_main() and chatbot_do_*() have the same method signature, which
 * works as described here.
 *
 * Input parameters:
 *   inc      - the number of words in the question
 *   inv      - an array of pointers to each word in the question
 *   response - a buffer to receive the response
 *   n        - the size of the response buffer
 *
 * The first word indicates the intent. If the intent is not recognised, the
 * chatbot should respond with "I do not understand [intent]." or similar, and
 * ignore the rest of the input.
 *
 * If the second word may be a part of speech that makes sense for the intent.
 *    - for WHAT, WHERE and WHO, it may be "is" or "are".
 *    - for SAVE, it may be "as" or "to".
 *    - for LOAD, it may be "from".
 * The word is otherwise ignored and may be omitted.
 *
 * The remainder of the input (including the second word, if it is not one of the
 * above) is the entity.
 *
 * The chatbot's answer should be stored in the output buffer, and be no longer
 * than n characters long (you can use snprintf() to do this). The contents of
 * this buffer will be printed by the main loop.
 *
 * The behaviour of the other functions is described individually in a comment
 * immediately before the function declaration.
 *
 * You can rename the chatbot and the user by changing chatbot_botname() and
 * chatbot_username(), respectively. The main loop will print the strings
 * returned by these functions at the start of each line.
 */
 
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <windows.h>
#include <time.h> 
#include "chat1002.h"

enum BOT_STATE {
	Default,
	AwaitingResponse,
	HangManState,
	RandomNoState,
	HelperState
};

enum BOT_STATE bot_state;
char prev_inv[MAX_INPUT][30];
int prev_inc = 0;
char chatbotname[100];
char username[100];

/* chatbot colors*/
HANDLE hConsole;
int textcol_default = 7;
int textcol_chatbot = 11;
int textcol_user = 10;
int texcol_prev = 7;

/*HangMan Game*/
#define MAX_GUESSES 10 /*  Number of guesses */
#define max_length 12  //max length of entered word
struct HangmanStruct {
	int welcome;
	int guess_count; //inits guesscount
	char hangman_word[max_length];
	char playerName[10];
	char winnerName[10];
	int singleormulti;
	int Mode;
	int word_isvalid;
	char p2gArr[max_length];
};

typedef struct HangmanStruct Hangman;
Hangman hangman;

/*Random Number Game*/
struct RandomNoStruct {
	int welcome;
	int isRandomNoGameEnd; //flag to check if random number game has ended
	int attempts_left;
	int userWin; //flag to check if user has won
	int n;
	int userGuess;
};
typedef struct RandomNoStruct RandomNo;
RandomNo randomNo;
/*
 * Get the name of the chatbot.
 *
 * Returns: the name of the chatbot as a null-terminated string
 */
const char *chatbot_botname() {
	return chatbotname;
}

/*
 * Get the name of the user.
 *
 * Returns: the name of the user as a null-terminated string
 */
const char *chatbot_username() {
	return username;
}

/*
 * Get the current text color for the chatbot
 *
 * Returns: the text color for the chatbot
 */
void chatbot_textcol_set_chatbot() {
	SetConsoleTextAttribute(hConsole, textcol_chatbot);
}

/*
 * Get the current text color for the user
 *
 * Returns: the text color for the user
 */
void chatbot_textcol_set_user() {
	SetConsoleTextAttribute(hConsole, textcol_user);
}

/*
 * Get the current text color for the user
 *
 * Returns: the text color for the user
 */
void chatbot_textcol_set_default() {
	SetConsoleTextAttribute(hConsole, textcol_default);
}

/*
 * Get a response to user input.
 *
 * See the comment at the top of the file for a description of how this
 * function is used.
 *
 * Returns:
 *   0, if the chatbot should continue chatting
 *   1, if the chatbot should stop (i.e. it detected the EXIT intent)
 */
int chatbot_main(int inc, char *inv[], char *response, int n) {
	/* check for empty input */
	if (inc < 1) {
		snprintf(response, n, "");
		return 0;
	}

	//Store the return value of chatbot_update
	int ret = chatbot_update(inc, inv, response, n);

	//Store the previous words the user typed in
	prev_inc = inc;
	for (int i = 0; i < inc; ++i) {
		strcpy(prev_inv[i], inv[i]);
	}
	return ret;
}

/*
 * Runs the appropriate functions depending on the chatbot's bot_state
 *
 * Returns:
 *   0, if the chatbot should continue chatting
 *   1, if the chatbot should stop (i.e. it detected the EXIT intent)
 */
int chatbot_update(int inc, char* inv[], char* response, int n) {
	if (bot_state == AwaitingResponse)
		return chatbot_record_response(inc, inv, response, n);
	else if (bot_state == HangManState) {
		return chatbot_do_HangMan(inc, inv, response, n);
	}
	else if (bot_state == RandomNoState) {
		return chatbot_do_randomNo(inc, inv, response, n);
	}
	else if (bot_state == HelperState) {
		return chatbot_do_help(inc, inv, response, n);
	}
	else
	{
		/* look for an intent and invoke the corresponding do_* function */
		if (chatbot_is_exit(inv[0]))
			return chatbot_do_exit(inc, inv, response, n);
		else if (chatbot_is_smalltalk(inv[0]))
			return chatbot_do_smalltalk(inc, inv, response, n);
		else if (chatbot_is_load(inv[0]))
			return chatbot_do_load(inc, inv, response, n);
		else if (chatbot_is_question(inv[0]))
			return chatbot_do_question(inc, inv, response, n);
		else if (chatbot_is_arith(inv[0]))
			return chatbot_do_arith(inc, inv, response, n);
		else if (chatbot_is_clear_screen(inv[0]))
			return chatbot_do_clear_screen(inc, inv, response, n);
		else if (chatbot_is_reset(inv[0]))
			return chatbot_do_reset(inc, inv, response, n, 1);
		else if (chatbot_is_save(inv[0]))
			return chatbot_do_save(inc, inv, response, n);
		else if (chatbot_is_name(inv[0]))
			return chatbot_do_name(inc, inv, response, n);
		else if (chatbot_is_change_color(inv[0]))
			return chatbot_do_change_color(inc, inv, response, n);
		else if (chatbot_is_hangman(inv[0]))
			return chatbot_do_HangMan(inc, inv, response, n);
		else if (chatbot_is_randomNo(inv[0]))
			return chatbot_do_randomNo(inc, inv, response, n);
		else if (chatbot_is_team25Info(inv[0]))
			return chatbot_do_team25Info(inc, inv, response, n);
		else if (chatbot_is_help(inv[0]))
			return chatbot_do_help(inc, inv, response, n);
		else {
			snprintf(response, n, "I don't understand \"%s\".", inv[0]);
			return 0;
		}
	}
}

/*
 * Determine whether an intent is EXIT.
 *
 * Input:
 *  intent - the intent
 *
 * Returns:
 *  1, if the intent is "exit" or "quit"
 *  0, otherwise
 */
int chatbot_is_exit(const char *intent) {
	return compare_token(intent, "exit") == 0 || compare_token(intent, "quit") == 0 || compare_token(intent, "goodbye") == 0 || compare_token(intent, "bye") == 0 || compare_token(intent, "bye-bye") == 0 || compare_token(intent, "byebye") == 0;
}


/*
 * Perform the EXIT intent.
 *
 * See the comment at the top of the file for a description of how this
 * function is used.
 *
 * Returns:
 *   0 (the chatbot always continues chatting after a question)
 */
int chatbot_do_exit(int inc, char *inv[], char *response, int n) {
	snprintf(response, n, "Goodbye!");
	return 1; 
}


/*
 * Determine whether an intent is LOAD.
 *
 * Input:
 *  intent - the intent
 *
 * Returns:
 *  1, if the intent is "load"
 *  0, otherwise
 */
int chatbot_is_load(const char *intent) {
	if (compare_token(intent, "load") == 0)
		return 1;
	return 0;
}


/*
 * Load a chatbot's knowledge base from a file.
 *
 * See the comment at the top of the file for a description of how this
 * function is used.
 *
 * Returns:
 *   0 (the chatbot always continues chatting after loading knowledge)
 */
int chatbot_do_load(int inc, char *inv[], char *response, int n) {
	if (inc < 2) {
		snprintf(response, n, "Invalid file name!");
		return 0;
	}

	char temp_name[] = "output.txt";
	char* file_name = temp_name;
	for (int i = 1; i < inc; i++) {
		if (compare_token(inv[i], "as") == 0) {
			continue;
		}
		file_name = inv[i];
		break;
	}

	FILE* file = fopen(file_name, "r");
	if (file == NULL) {
		snprintf(response, n, "File could not be found!");
		return 0;
	}

	knowledge_read(file);
	fclose(file);
	snprintf(response, n, "Loaded file!");
	return 0;
}


/*
 * Determine whether an intent is a question.
 *
 * Input:
 *  intent - the intent
 *
 * Returns:
 *  1, if the intent is "what", "where", or "who"
 *  0, otherwise
 */
int chatbot_is_question(const char *intent) {
	int is_question = 0;
	if (compare_token(intent, "who") == 0 || compare_token(intent, "what") == 0 || compare_token(intent, "why") == 0 ||
		compare_token(intent, "when") == 0 ||compare_token(intent, "which") == 0 || compare_token(intent, "where") == 0 || compare_token(intent, "how") == 0) {
		is_question = 1;
	}
	return is_question;
}


/*
 * Answer a question.
 *
 * inv[0] contains the the question word.
 * inv[1] may contain "is" or "are"; if so, it is skipped.
 * The remainder of the words form the entity.
 *
 * See the comment at the top of the file for a description of how this
 * function is used.
 *
 * Returns:
 *   0 (the chatbot always continues chatting after a question)
 */
int chatbot_do_question(int inc, char *inv[], char *response, int n) {
	if (inc < 2) {
		snprintf(response, n, "%s?", inv[0]);
		return 0;
	}

	char entity[MAX_INPUT] = "\0";
	for (int i = (compare_token(inv[1], "is") == 0 || compare_token(inv[1], "are") == 0) ? 2 : 1
		; i < inc; ++i) {
		strcat(entity, inv[i]);
		if(i != inc - 1)
			strcat(entity, " ");
	}
	int flag = knowledge_get(inv[0], entity, response, n);
	if (flag == KB_OK) {
		//Do nothing
	}
	else if (flag == KB_NOTFOUND) {
		snprintf(response, n, "I don't know. ");
		for (int i = 0; i < inc; ++i) {
			if (compare_token(inv[i], "your") == 0 || compare_token(inv[i], "ur") == 0)
				snprintf(response + strlen(response), n, "%s ", "my");
			else if (compare_token(inv[i], "my") == 0)
				snprintf(response + strlen(response), n, "%s ", "your");
			else if (compare_token(inv[i], "yours") == 0)
				snprintf(response + strlen(response), n, "%s ", "mine");
			else if (compare_token(inv[i], "mine") == 0)
				snprintf(response + strlen(response), n, "%s ", "yours");
			else
				snprintf(response + strlen(response), n, "%s ", inv[i]);
		}
		snprintf(response + strlen(response) - 1, n, "?");
		bot_state = AwaitingResponse;
	}
	else {
		snprintf(response, n, "I don't understand!");
	}
	return 0; 
}

/*
 * Records a response to a question
 *
 * inv[0] contains the the question word.
 * inv[1] may contain "is" or "are"; if so, it is skipped.
 * The remainder of the words form the entity.
 *
 *
 * Returns:
 *	0 if knowledge was recorded
 *	1 if knowledge was unable to be recorded
 */
int chatbot_record_response(int inc, char* inv[], char* response, int n) {
	//Reset bot state to default
	bot_state = Default;
	if (inc == 0) {
		snprintf(response, n, ":-(");
		return 1;
	}
	if (prev_inc == 0 || prev_inv[0] == NULL || prev_inv[1] == NULL)
		return 1;
	//Combine prev_inv to form the entity question
	char entity[MAX_INPUT] = "\0";
	for (int i = (compare_token(prev_inv[1], "is") == 0 || compare_token(prev_inv[1], "are") == 0) ? 2 : 1
		; i < prev_inc; ++i) {
		strcat(entity, prev_inv[i]);
		if (i != prev_inc - 1)
			strcat(entity, " ");
	}
	//Combine inv to form the answer to the previous question
	char answer[MAX_INPUT] = "\0";
	for (int i = 0; i < inc; ++i) {
		strcat(answer, inv[i]);
		if (i != inc - 1)
			strcat(answer, " ");
	}
	int flag = knowledge_put(prev_inv[0], entity, answer);
	if (flag == KB_OK) {
		snprintf(response, n, "Thank you.");
		return 0;
	}
	if (flag == KB_NOMEM) {
		snprintf(response, n, "Looks like I'm out of memory!");
		return 1;
	}
	return 0;
}

/*
* Extra Function 1
* Determine whether an intent is a valid mathematical operation in order to perform the common arithmetical operations.
*
* Input:
* intent - the intent
*
* Returns:
*  1, if the intent is "add", "subtract", "mutiply", "divide", "mod", etc
*  0, otherwise
*/
int chatbot_is_arith(const char* intent) {
	int is_arith = 0;
	if (compare_token(intent, "add") == 0 || compare_token(intent, "subtract") == 0 || compare_token(intent, "multiply") == 0 || compare_token(intent, "divide") == 0 || compare_token(intent, "mod") == 0 ||
		compare_token(intent, "modulo") == 0 || compare_token(intent, "absolute") == 0 || compare_token(intent, "abs") == 0 || compare_token(intent, "square") == 0 || compare_token(intent, "^2") == 0 ||
		compare_token(intent, "cube") == 0 || compare_token(intent, "^3") == 0 || compare_token(intent, "sqrt") == 0 || compare_token(intent, "^1/2") == 0 || compare_token(intent, "cbrt") == 0 || compare_token(intent, "^1/3") == 0) {
		is_arith = 1;
	}
	return is_arith;
}

/*
 * The chatbot will perform arithmetic operations and returns result to user.
 *
 * inv[0] contains the the question word.
 * inv[1] contains first operand
 * inv[2] may contain "AND" or 2nd operand depending on what the user has typed
 * inv[3] may contain 2nd operand if user decides to type in AND as inv[2]
 *
 * See the comment at the top of the file for a description of how this
 * function is used.
 *
 * Returns:
 *   0 (the chatbot always continues chatting after a question)
 */
int chatbot_do_arith(int inc, char* inv[], char* response, int n) {
	float n1 = 0.0, n2 = 0.0;
	//Display error if there are no operands.
	if ((inc - 1) == 0) {
		snprintf(response, n, "Sorry, there are no operands to perform %s operation!", inv[0]);
	}
	//perform selected operations on 1 operand such as mod, square, cube, square root and cube root
	else if ((inc - 1) == 1) {
		if (compare_token(inv[0], "add") == 0 || compare_token(inv[0], "subtract") == 0 || compare_token(inv[0], "multiply") == 0 || compare_token(inv[0], "divide") == 0 || compare_token(inv[0], "mod") == 0 || compare_token(inv[0], "modulo") == 0) {
			snprintf(response, n, "Sorry, the %s operation requires 2 operands!", inv[0]);
		}
		else {
			//convert number to float if it is a proper number
			if (checkNum1(inv)) {
				n1 = (float)atof(inv[1]);
				//perform arithmetic operations with 1 operand only
				do_arith_1_operand(inv, n1, response, n);
			}
			else {
				snprintf(response, n, "%s", "Sorry, please enter a valid number for 1st operand!");
			}
		}
		return 0;
	}
	//perform selected operations on 2 operands such as mod, square, cube, square root and cube root
	else if ((inc - 1) == 2) {
		//convert number to float if it is a proper number
		if (!checkNum1(inv)) {
			snprintf(response, n, "%s", "Sorry, please enter a valid number for 1st operand!");
		}
		if (!checkNum2(inv)) {
			snprintf(response, n, "%s", "Sorry, please enter a valid number for 2nd operand!");
		}
		if (!checkNum1(inv) && !checkNum2(inv)) {
			snprintf(response, n, "%s", "Sorry, please enter a valid number for both operands!");
		}
		else if (checkNum1(inv) && checkNum2(inv)) {
			n1 = (float)atof(inv[1]);
			n2 = (float)atof(inv[2]);
			//perform arithmetic operations involving 2 operands
			do_arith_2_operand(inv, n1, n2, response, n);
		}
	}
	//if using the format 1st operand AND 2nd operand
	else if ((inc - 1) == 3 && compare_token(inv[2], "and") == 0) {
		if (!checkNum1(inv)) {
			snprintf(response, n, "%s", "Sorry, please enter a valid number for 1st operand!");
		}
		if (!checkNum1(inv)) {
			snprintf(response, n, "%s", "Sorry, please enter a valid number for 2nd operand!");
		}
		if (!checkNum1(inv) && !checkNum2WithAnd(inv)) {
			snprintf(response, n, "%s", "Sorry, please enter a valid number for both operands!");
		}
		else if (checkNum1(inv) && checkNum2WithAnd(inv)) {
			n1 = (float)atof(inv[1]);
			n2 = (float)atof(inv[3]);
			do_arith_2_operand(inv, n1, n2, response, n);
		}
	}
	else {
		snprintf(response, n, "%s", "Sorry, please enter 2 proper operands in a format:\n-operator operand1 operand2\n-operator operand1 AND operand2!");
	}
	return 0;
}

//check if first operand is a number
int checkNum1(char *inv[]) {
	int isNum1 = 1, noOfDecimalPointN1 = 0;
	if (inv[1][0] == '-') {
		//for negative integers, check the digits starting from index 1
		for (int i = 1; i < strlen(inv[1]); i++) {
			if (!isdigit(inv[1][i])) {
				//check if number contains only 1 acceptable decimal point
				if (inv[1][i] == '.') {
					noOfDecimalPointN1++;
				}
				//reject the number if contains more than 1 acceptable decimal point
				else {
					isNum1 = 0;
				}
			}
		}
		//reject the number if contains more than 1 proper decimal point
		if (noOfDecimalPointN1 > 1) {
			isNum1 = 0;
		}
	}
	else {
		//for positive integers, check the digits starting from index 0
		for (int i = 0; i < strlen(inv[1]); i++) {
			if (!isdigit(inv[1][i])) {
				//check if number contains only 1 acceptable decimal point
				if (inv[1][i] == '.') {
					noOfDecimalPointN1++;
				}
				else {
					isNum1 = 0;
				}
			}
		}
		//reject the number if contains more than 1 decimal point
		if (noOfDecimalPointN1 > 1) {
			isNum1 = 0;
		}
	}
	return isNum1;
}

//check if second operand is a number
int checkNum2(char* inv[]) {
	int isNum2 = 1, noOfDecimalPointN2 = 0;
	if (inv[2][0] == '-') {
		for (int i = 1; i < strlen(inv[2]); i++) {
			if (!isdigit(inv[2][i])) {
				//check if number contains only 1 acceptable decimal point
				if (inv[2][i] == '.') {
					noOfDecimalPointN2++;
				}
				//reject the number if contains more than 1 acceptable decimal point
				else {
					isNum2 = 0;
				}
			}
		}
		//reject the number if contains more than 1 decimal point
		if (noOfDecimalPointN2 > 1) {
			isNum2 = 0;
		}
	}
	//for positive integers, check the digits from index 0
	else {
		for (int i = 0; i < strlen(inv[2]); i++) {
			if (!isdigit(inv[2][i])) {
				//check if number contains only 1 acceptable decimal point
				if (inv[2][i] == '.') {
					noOfDecimalPointN2++;
				}
				//reject the number if contains more than 1 acceptable decimal point
				else {
					isNum2 = 0;
				}
			}
		}
		//reject the number if contains more than 1 decimal point
		if (noOfDecimalPointN2 > 1) {
			isNum2 = 0;
		}
	}
	return isNum2;
}

//check if second operand is a number, if there is AND before second operand
int checkNum2WithAnd(char* inv[]) {
	int isNum2 = 1, noOfDecimalPointN2 = 0;
	if (inv[3][0] == '-') {
		for (int i = 1; i < strlen(inv[3]); i++) {
			if (!isdigit(inv[3][i])) {
				//check if number contains only 1 acceptable decimal point
				if (inv[3][i] == '.') {
					noOfDecimalPointN2++;
				}
				//reject the number if contains more than 1 acceptable decimal point
				else {
					isNum2 = 0;
				}
			}
		}
		//reject the number if contains more than 1 decimal point
		if (noOfDecimalPointN2 > 1) {
			isNum2 = 0;
		}
	}
	//for positive integers, check the digits from index 0
	else {
		for (int i = 0; i < strlen(inv[3]); i++) {
			if (!isdigit(inv[3][i])) {
				//check if number contains only 1 acceptable decimal point
				if (inv[3][i] == '.') {
					noOfDecimalPointN2++;
				}
				//reject the number if contains more than 1 acceptable decimal point
				else {
					isNum2 = 0;
				}
			}
		}
		//reject the number if contains more than 1 decimal point
		if (noOfDecimalPointN2 > 1) {
			isNum2 = 0;
		}
	}
	return isNum2;
}

//This is the arithmetic function to perform arithmetic operations with only 1 operand: abs, square, cube, sqrt, cbrt
void do_arith_1_operand(char *inv[], float n1, char* response, int n) {
	float absn1, sq1, cube1, sqrt1, cbrt1 = 0.0;
	if (compare_token(inv[0], "absolute") == 0 || compare_token(inv[0], "abs") == 0) {
		//display red text to tell user that it is a specific arithmetic operation
		SetConsoleTextAttribute(hConsole, 12);
		printf("This is a single-operand absolute operation!\n");
		if (n1 < 0) {
			absn1 = -n1;
		}
		else {
			absn1 = n1;
		}
		snprintf(response, n, "%s", inv[0]);
		SetConsoleTextAttribute(hConsole, 6);
		printf("%s  %s %.3f = %.3f", chatbotname, inv[0], n1, absn1);
	}
	//perform square operation
	else if (compare_token(inv[0], "square") == 0 || compare_token(inv[0], "^2") == 0) {
		//display red text to tell user that it is a specific arithmetic operation
		SetConsoleTextAttribute(hConsole, 12);
		printf("This is a single-operand square operation!\n");
		sq1 = (n1 * n1);
		snprintf(response, n, "%s", inv[0]);
		SetConsoleTextAttribute(hConsole, 6);
		printf("%s  %s %.3f = %.3f", chatbotname, inv[0], n1, sq1);
	}
	//perform cube operation
	else if (compare_token(inv[0], "cube") == 0 || compare_token(inv[0], "^3") == 0) {
		//display red text to tell user that it is a specific arithmetic operation
		SetConsoleTextAttribute(hConsole, 12);
		printf("This is a single-operand cube operation!\n");
		cube1 = (n1 * n1 * n1);
		snprintf(response, n, "%s", inv[0]);
		SetConsoleTextAttribute(hConsole, 6);
		printf("%s  %s %.3f = %.3f", chatbotname, inv[0], n1, cube1);
	}
	//perform square root operation
	else if (compare_token(inv[0], "sqrt") == 0 || compare_token(inv[0], "^1/2") == 0) {
		//display red text to tell user that it is a specific arithmetic operation
		SetConsoleTextAttribute(hConsole, 12);
		printf("This is a single-operand square root operation!\n");
		sqrt1 = (float)sqrt(n1);
		snprintf(response, n, "%s", inv[0]);
		SetConsoleTextAttribute(hConsole, 6);
		printf("%s  %s %.3f = %.3f", chatbotname, inv[0], n1, sqrt1);
	}
	//perform cube root operation
	else if (compare_token(inv[0], "cbrt") == 0 || compare_token(inv[0], "^1/3") == 0) {
		//display red text to tell user that it is a specific arithmetic operation
		SetConsoleTextAttribute(hConsole, 12);
		printf("This is a single-operand cube root operation!\n");
		cbrt1 = (float)cbrt(n1);
		snprintf(response, n, "%s", inv[0]);
		SetConsoleTextAttribute(hConsole, 6);
		printf("%s  %s %.3f = %.3f", chatbotname, inv[0], n1, cbrt1);
	}
}

//This is the arithmetic function to perform arithmetic operations with 2 operands
void do_arith_2_operand(char* inv[], float n1, float n2, char* response, int n) {
	float absn1, absn2, sq1, sq2, cube1, cube2, sqrt1, sqrt2, cbrt1, cbrt2, t = 0.0;
	int r = 0;
	if (compare_token(inv[0], "absolute") == 0 || compare_token(inv[0], "abs") == 0) {
		//display red text to tell user that it is a specific arithmetic operation
		SetConsoleTextAttribute(hConsole, 12);
		printf("This is a double-operand absolute operation!\n");
		if (n1 < 0) {
			absn1 = -n1;
		}
		if (n1 >= 0) {
			absn1 = n1;
		}
		if (n2 < 0) {
			absn2 = -n2;
		}
		if (n2 >= 0) {
			absn2 = n2;
		}
		snprintf(response, n, "%s", inv[0]);
		SetConsoleTextAttribute(hConsole, 6);
		printf("%s  %s %.3f AND %.3f = %.3f AND %.3f", chatbotname, inv[0], n1, n2, absn1, absn2);
	}
	//perform square operation
	else if (compare_token(inv[0], "square") == 0 || compare_token(inv[0], "^2") == 0) {
		//display red text to tell user that it is a specific arithmetic operation
		SetConsoleTextAttribute(hConsole, 12);
		printf("This is an double-operand square operation!\n");
		sq1 = (n1 * n1);
		sq2 = (n2 * n2);
		snprintf(response, n, "%s", inv[0]);
		SetConsoleTextAttribute(hConsole, 6);
		printf("%s  %s %.3f AND %.3f = %.3f AND %.3f", chatbotname, inv[0], n1, n2, sq1, sq2);
	}
	else if (compare_token(inv[0], "cube") == 0 || compare_token(inv[0], "^3") == 0) {
		//display red text to tell user that it is a specific arithmetic operation
		SetConsoleTextAttribute(hConsole, 12);
		printf("This is a double-operand cube operation!\n");
		cube1 = (n1 * n1 * n1);
		cube2 = (n2 * n2 * n2);
		snprintf(response, n, "%s", inv[0]);
		SetConsoleTextAttribute(hConsole, 6);
		printf("%s  %s %.3f AND %.3f = %.3f AND %.3f", chatbotname, inv[0], n1, n2, cube1, cube2);
	}
	else if (compare_token(inv[0], "sqrt") == 0 || compare_token(inv[0], "^1/2") == 0) {
		//display red text to tell user that it is a specific arithmetic operation
		SetConsoleTextAttribute(hConsole, 12);
		printf("This is a double-operand sqrt operation!\n");
		sqrt1 = (float)sqrt(n1);
		sqrt2 = (float)sqrt(n2);
		snprintf(response, n, "%s", inv[0]);
		SetConsoleTextAttribute(hConsole, 6);
		printf("%s  %s %.3f AND %.3f = %.3f AND %.3f", chatbotname, inv[0], n1, n2, sqrt1, sqrt2);
	}
	else if (compare_token(inv[0], "cbrt") == 0 || compare_token(inv[0], "^1/3") == 0) {
		//display red text to tell user that it is a specific arithmetic operation
		SetConsoleTextAttribute(hConsole, 12);
		printf("This is a double-operand cbrt operation!\n");
		cbrt1 = (float)cbrt(n1);
		cbrt2 = (float)cbrt(n2);
		snprintf(response, n, "%s", inv[0]);
		SetConsoleTextAttribute(hConsole, 6);
		printf("%s  %s %.3f AND %.3f = %.3f AND %.3f", chatbotname, inv[0], n1, n2, cbrt1, cbrt2);
	}
	else if (compare_token(inv[0], "add") == 0) {
		//display red text to tell user that it is a specific arithmetic operation
		SetConsoleTextAttribute(hConsole, 12);
		printf("This is a double-operand %s operation!\n", inv[0]);
		t = n1 + n2;
		snprintf(response, n, "%s", inv[0]);
		SetConsoleTextAttribute(hConsole, 6);
		printf("%s  %s %.3f AND %.3f = %.3f", chatbotname, inv[0], n1, n2, t);
	}
	else if (compare_token(inv[0], "subtract") == 0) {
		//display red text to tell user that it is a specific arithmetic operation
		SetConsoleTextAttribute(hConsole, 12);
		printf("This is a double-operand %s operation!\n", inv[0]);
		t = n1 - n2;
		snprintf(response, n, "%s", inv[0]);
		SetConsoleTextAttribute(hConsole, 6);
		printf("%s  %s %.3f AND %.3f = %.3f", chatbotname, inv[0], n1, n2, t);
	}
	else if (compare_token(inv[0], "multiply") == 0) {
		//display red text to tell user that it is a specific arithmetic operation
		SetConsoleTextAttribute(hConsole, 12);
		printf("This is a double-operand %s operation!\n", inv[0]);
		t = n1 * n2;
		snprintf(response, n, "%s", inv[0]);
		SetConsoleTextAttribute(hConsole, 6);
		printf("%s  %s %.3f AND %.3f = %.3f", chatbotname, inv[0], n1, n2, t);
	}
	else if (compare_token(inv[0], "divide") == 0) {
		//display red text to tell user that it is a specific arithmetic operation
		SetConsoleTextAttribute(hConsole, 12);
		printf("This is a double-operand %s operation!\n", inv[0]);
		if (n2 == 0.0) {
			snprintf(response, n, "%s", "Error, cannot divide by 0!");
		}
		else {
			t = n1 / n2;
			snprintf(response, n, "%s", inv[0]);
			SetConsoleTextAttribute(hConsole, 6);
			printf("%s  %s %.3f AND %.3f = %.3f", chatbotname, inv[0], n1, n2, t);
		}
	}
	else if (compare_token(inv[0], "modulo") == 0 || compare_token(inv[0], "mod") == 0) {
		if (n2 == 0.0) {
			snprintf(response, n, "%s", "Error, cannot divide by 0!");
		}
		else {
			r = (int)n1 % (int)n2;
			snprintf(response, n, "%s", inv[0]);
			SetConsoleTextAttribute(hConsole, 6);
			printf("%s  %s %.3f AND %.3f = %d", chatbotname, inv[0], n1, n2, r);
		}
	}
}

/*
 * Determine whether an intent is CLEARSCREEN.
 *
 * Input:
 *  intent - the intent
 *
 * Returns:
 *  1, if the intent is "clearscreen"
 *  0, otherwise
 */
int chatbot_is_clear_screen(const char* intent) {
	if (compare_token(intent, "clear") == 0 || compare_token(intent, "clearscreen") == 0) {
		return 1;
	}
	return 0;
}

/*
 * Clears the console screen
 *
 * Returns:
 *   0 after clearing the screen
 */
int chatbot_do_clear_screen(int inc, char* inv[], char* response, int n) {
	system("cls");
	snprintf(response, n, "I have cleared the screen!");
	return 0;
}

/*
 * Determine whether an intent is RESET.
 *
 * Input:
 *  intent - the intent
 *
 * Returns:
 *  1, if the intent is "reset"
 *  0, otherwise
 */
int chatbot_is_reset(const char *intent) {
	if (compare_token(intent, "reset") == 0) {
		return 1;
	}
	return 0;
}


/*
 * Reset the chatbot.
 *
 * See the comment at the top of the file for a description of how this
 * function is used.
 *
 * Returns:
 *   0 (the chatbot always continues chatting after begin reset)
 */
int chatbot_do_reset(int inc, char *inv[], char *response, int n, int display_reset_msg) {
	/* reset chatbot knowledge */
	if(hConsole == NULL)
		hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	if(display_reset_msg == 1)
		snprintf(response, n, "Chatbot has been reset!");
	hangman.welcome = 0;
	hangman.word_isvalid = 0;
	randomNo.welcome = 0;
	randomNo.userWin = 0;
	knowledge_reset();
	strcpy(chatbotname, "chatbot");
	strcpy(username, "user name");
	bot_state = Default;
	return 0;	 
}


/*
 * Determine whether an intent is SAVE.
 *
 * Input:
 *  intent - the intent
 *
 * Returns:
 *  1, if the intent is "what", "where", or "who"
 *  0, otherwise
 */
int chatbot_is_save(const char *intent) {
	if (compare_token(intent, "save") == 0) { 
		return 1;
	}	
	return 0;
}
/*
 * Save the chatbot's knowledge to a file.
 *
 * See the comment at the top of the file for a description of how this
 * function is used.
 *
 * Returns:
 *   0 (the chatbot always continues chatting after saving knowledge)
 */
int chatbot_do_save(int inc, char* inv[], char* response, int n) {
	if (inc < 2) {
		snprintf(response, n, "Invalid file name!");
		return 0;
	}

	char temp_name[] = "output.txt";
	char* file_name = temp_name;
	for (int i = 1; i < inc; i++) {
		if (compare_token(inv[i], "as") == 0) {
			continue;
		}
		file_name = inv[i];
		break;
	}

	FILE* file = fopen(file_name, "w");
	if (file == NULL) {
		snprintf(response, n, "Invalid file path!");
		return 0;
	}
	knowledge_write(file);
	fclose(file);
	snprintf(response, n, "My knowledge has been saved to %s!", file_name);
	return 0;
}

/*
 * Checks if the intent is to change the name(regardless bot or user)
 *
 * Input:
 *  intent - the intent
 *
 * Returns:
 *  1, if the intent is to change the username/chatbot name
 *  0, otherwise
 */
int chatbot_is_name(const char* intent) {
	if (compare_token(intent, "username") == 0 || compare_token(intent, "name") == 0 || compare_token(intent, "botname") == 0 || compare_token(intent, "chatbotname") == 0) {
		return 1;
	}
	return 0;
}

/*
 * Changes the chatbotname or username depending on what is entered
 *
 * Returns:
 *   0, if the name was successfully changed
 */
int chatbot_do_name(int inc, char* inv[], char* response, int n) {
	if (inc < 2) {
		snprintf(response, n, "%s", "Sorry, please enter a name!");
		return 0;
	}
	for (int i = 1; i < inc; i++) {
		if (inc >= 2) {
			//check if there is name
			if (compare_token(inv[i], "is") == 0) {
				continue;
			}
			if (compare_token(inv[0], "botname") == 0 || compare_token(inv[0], "chatbotname") == 0) {
				strcpy(chatbotname, inv[i]);
				snprintf(response, n, "Ok, my name is now %s.", chatbotname);
			}
			else if (compare_token(inv[0], "user") == 0 || compare_token(inv[0], "username") == 0 || compare_token(inv[0], "name") == 0) {
				strcpy(username, inv[i]);
				snprintf(response, n, "Ok, your name is %s.", username);
			}
			break;
		}
		//Display error message if no name is being typed
		else {
			snprintf(response, n, "%s", "Sorry, please enter a name!");
		}
	}
	return 0;
}

/*
 * Check whether intent is to open help menu.
 *
 * Input:
 *  intent - the intent
 *
 * Returns:
 *  1, if the intent is to show help
 *  0, otherwise
 */
int chatbot_is_help(const char* intent) {
	if (compare_token(intent, "show") == 0 || compare_token(intent, "help") == 0) {
		return 1;
	}
	return 0;
}

/*
 * Displays description for all the functions in the chatbot when the relevant keyword or corresponding number is entered.
 *
 * Returns:
 *   0, if help was successfully shown
 */
int chatbot_do_help(int inc, char* inv[], char* response, int n) {
	if (compare_token(inv[0], "help") == 0) {
		//change txtcolor to white gray to indicate that bot is in HelperState
		texcol_prev = textcol_chatbot;
		textcol_chatbot = 7;
		helper_menu(inc, inv, response, n);
		bot_state = HelperState;
		return 0;
	}
	if (compare_token(inv[0], "show") == 0 && compare_token(inv[1], "help") == 0) {
		//change txtcolor to white gray to indicate that bot is in HelperState
		texcol_prev = textcol_chatbot;
		textcol_chatbot = 7;
		helper_menu(inc, inv, response, n);
		bot_state = HelperState;
		return 0;
	}
	if (bot_state == HelperState) {
		snprintf(response, n, "Invalid help command!");
		if (compare_token(inv[0], "math") == 0 || compare_token(inv[0], "1") == 0) {
			snprintf(response, n, "\n\nmath functions use this format:\nadd(2 operands)\nsubtract(2 operands)\nmultiply(2 operands)\ndivide(2 operands)\nmodulo(2 operands)\n"
				"abs(1 or 2 operands)\nsquare(1 or 2 operands)\ncube(1 or 2 operands)\nsqrt(1 or 2 operands)\ncbrt(1 or 2 operands)\n\n");
			return 0;
		}
		if (compare_token(inv[0], "hangman") == 0 || compare_token(inv[0], "2") == 0) {
			snprintf(response, n, "\n\nLoad Hangman Game with \"hangman\" ! \n"
				"To quit the game, just enter \"quit\". \n\n");
			return 0;
		}
		if (compare_token(inv[0], "color") == 0 || compare_token(inv[0], "3") == 0 || compare_token(inv[0], "colour") == 0) {
			snprintf(response, n, "\n\nChange color using \"color ___\"\n\n");
			return 0;
		}
		if (compare_token(inv[0], "clearscreen") == 0 || compare_token(inv[0], "clear") == 0 || compare_token(inv[0], "4") == 0) {
			snprintf(response, n, "\n\nClear screen with \"clear\" or \"clearscreen\"\n\n");
			return 0;
		}
		if (compare_token(inv[0], "quit") == 0 || compare_token(inv[0], "5") == 0) {
			snprintf(response, n, "\n\nQuit the Chatbot App with \"quit\"\n"
				"To quit the Help Menu, please key \"helpquit\" instead\n");
			return 0;
		}
		if (compare_token(inv[0], "load") == 0 || compare_token(inv[0], "6") == 0) {
			snprintf(response, n, "\n\nLoad chatbot knowledge base with \"load ___\"\n\n");
			return 0;
		}
		if (compare_token(inv[0], "random") == 0 || compare_token(inv[0], "7") == 0) {
			snprintf(response, n, "\n\nLoad the Random Number Guessing Game with \"randomno\" or \"randomnumber\" ! \n"
				"To quit the game, just enter \"quit\". \n\n");
			return 0;
		}
		if (compare_token(inv[0], "question") == 0 || compare_token(inv[0], "8") == 0) {
			snprintf(response, n, "\n\nAsk me a question starting with who / what / when / why / where / which / how\n"
				"I'll learn from you if I don't know the answer.\n\n");
			return 0;
		}
		if (compare_token(inv[0], "team") == 0 || compare_token(inv[0], "9") == 0) {
			snprintf(response, n, "\n\nKnow more about the team who worked on this project with: \"teaminfo\" or \"teamcredits\"\n\n");
			return 0;
		}
		if (compare_token(inv[0], "name") == 0 || compare_token(inv[0], "10") == 0) {
			snprintf(response, n, "\n\nChange my name by indicating \"chatbotname ___\" or \"botname ___\"\n"
				"Customize your username by indicating \"username ___\" or \"name ___\"\n\n");
			return 0;
		}
		if (compare_token(inv[0], "reset") == 0 || compare_token(inv[0], "11") == 0) {
			snprintf(response, n, "\n\nReset my memory through \"reset\"\n\n");
			return 0;
		}
		if (compare_token(inv[0], "save") == 0 || compare_token(inv[0], "12") == 0) {
			snprintf(response, n, "\n\nSave chatbot knowledge base to a file base \"save ___\"\n\n");
			return 0;
		}
		if (compare_token(inv[0], "smalltalk") == 0 || compare_token(inv[0], "13") == 0) {
			snprintf(response, n, "\n\nTalk with me by starting your sentence with its / the / hi / hello\n\n");
			return 0;
		}
		if (compare_token(inv[0], "helpquit") == 0) {
			snprintf(response, n, "\n\nExited from help menu!\n\n");
			bot_state = Default;
			textcol_chatbot = texcol_prev;
			return 0;
		}
		//revert to default color 11(turquoise) if out of HelperState
		if (bot_state == Default) {
			textcol_chatbot = texcol_prev;
		}
		if (compare_token(inv[0], "help") == 0 || compare_token(inv[0], "menu") == 0) {
			helper_menu(inc, inv, response, n);
			return 0;
		}
		//exception handling for non defined cases
		else {
			snprintf(response, n, "\n\nI don't understand what you're asking.\n"
				"Please refer to the help menu above or type \"help\" or \"menu\".\n"
				"To quit help menu, please enter \"helpquit\".\n\n");
		}
	}
	return 0;
}

/*this function is used to display the helper menu
	used in chatbot_do_help to display the help menu when "help" is keyed.
*/
int helper_menu(int inc, char* inv[], char* response, int n) {
	char helpsplash[7][63] = {
		{" _   _ ______ ______ _____    _    _ ______ _      _____ ___  "},
	{"| \\ | |  ____|  ____|  __ \\  | |  | |  ____| |    |  __ \\__ \\ "},
	{"|  \\| | |__  | |__  | |  | | | |__| | |__  | |    | |__) | ) |"},
	{"|   ` |  __| |  __| | |  | | |  __  |  __| | |    |  ___/ / / "},
	{"| |\\  | |____| |____| |__| | | |  | | |____| |____| |    |_|"},
	{"|_| \\_|______|______|_____/  |_|  |_|______|______|_|    (_)"}

	};
	snprintf(response, n, "\n");
	for (int i = 0; i < 7; ++i) {
		snprintf(response + strlen(response), n, "%s\n", helpsplash[i]);
	}
	snprintf(response + strlen(response), n,
		"\n***Key in the following keywords or their corresponding integers to learn more***\n\n"
		"          1. math\n"
		"          2. hangman\n"
		"          3. color\n"
		"          4. clearscreen\n"
		"          5. quit\n"
		"          6. load\n"
		"          7. random\n"
		"          8. question\n"
		"          9. team\n"
		"          10. name\n"
		"          11. reset\n"
		"          12. save\n"
		"          13. smalltalk\n"
		"\n*** KEY helpquit TO QUIT MENU ***\n\n");
	return 0;
}
 
/*
 * Determine which an intent is smalltalk.
 *
 *
 * Input:
 *  intent - the intent
 *
 * Returns:
 *  1, if the intent is the first word of one of the smalltalk phrases
 *  0, otherwise
 */
int chatbot_is_smalltalk(const char *intent) {
	int is_smalltalk = 0;
	if (compare_token(intent, "hi") == 0 || compare_token(intent, "its") == 0 || compare_token(intent, "the") == 0 || compare_token(intent, "hello") == 0 || compare_token(intent, "hey") == 0) {
		is_smalltalk = 1;
	}
	return is_smalltalk;
}

/*
 * Respond to smalltalk.
 *
 * See the comment at the top of the file for a description of how this
 * function is used.
 *
 * Returns:
 *   0, if the chatbot should continue chatting
 *   1, if the chatbot should stop chatting (e.g. the smalltalk was "goodbye" etc.)
 */
int chatbot_do_smalltalk(int inc, char *inv[], char *response, int n) {
	if (compare_token(inv[0], "its") == 0 || compare_token(inv[0], "the") == 0) {
		snprintf(response, n, "Indeed, it is.");
	}
	if (compare_token(inv[0], "hi") == 0 || compare_token(inv[0], "hello") == 0 || compare_token(inv[0], "hey") == 0) {
		snprintf(response, n, "Good day!");
	}
	return 0;
}

/*
 * Determine which an intent is change color.
 *
 *
 * Returns:
 *  1, if the intent is to change the username/chatbot text color
 *  0, otherwise
 */
int chatbot_is_change_color(const char* intent) {
	if (compare_token(intent, "color") == 0 || compare_token(intent, "colour") == 0 
		|| compare_token(intent, "changecolor") == 0 || compare_token(intent, "changecolour") == 0
		|| compare_token(intent, "col") == 0) {
		return 1;
	}
	return 0;
}

/*
 * Change the text color printed to the console
 *
 * Returns:
 *   0, if the text color was successfully changed
 */
int chatbot_do_change_color(int inc, char* inv[], char* response, int n) {

	//Return if wrong amount of paramters are given
	if (inc < 2) {
		snprintf(response, n, "Choose from the following colors: RED, BLUE, GREEN, CYAN, PURPLE, YELLOW, WHITE, GREY");
		return 0;
	}

	int color_word_index = 2;	//The index of the actual color string in inv[]
	int type = 0;				//0 = chatbot, 1 = user
	if (compare_token(inv[1], "user") == 0)
		type = 1;
	else if (compare_token(inv[1], "chatbot") != 0 && compare_token(inv[1], "bot") != 0) {
		color_word_index = 1;
	}

	//Check selected color string and set the appropriate color ID
	int selected_col = 7;
	if (compare_token(inv[color_word_index], "red") == 0)
		selected_col = 12;
	else if (compare_token(inv[color_word_index], "blue") == 0)
		selected_col = 9;
	else if (compare_token(inv[color_word_index], "green") == 0)
		selected_col = 10;
	else if (compare_token(inv[color_word_index], "cyan") == 0)
		selected_col = 11;
	else if (compare_token(inv[color_word_index], "purple") == 0 || compare_token(inv[color_word_index], "magenta") == 0)
		selected_col = 13;
	else if (compare_token(inv[color_word_index], "yellow") == 0)
		selected_col = 14;
	else if (compare_token(inv[color_word_index], "white") == 0)
		selected_col = 15;
	else if (compare_token(inv[color_word_index], "grey") == 0 || compare_token(inv[color_word_index], "gray") == 0)
		selected_col = 8;
	else {
		snprintf(response, n, "I don't recognise that color! Choose from the following colors: RED, BLUE, GREEN, CYAN, PURPLE, YELLOW, WHITE, GREY");
		return 0;
	}

	//Set the chatbot/user color accordingly
	if (type == 0)
		textcol_chatbot = selected_col;
	else
		textcol_user = selected_col;

	snprintf(response, n, "Color changed!");
	return 0;
}

/*
 * Determine when an intent is play hangman
 *
 *
 * Input:
 *  intent - the intent
 *
 * Returns:
 *  1, if the intent is play hangman
 *  0, otherwise
 */
int chatbot_is_hangman(const char* intent) {
	if (compare_token(intent, "hangman") == 0) {
		return 1;
	}
	return 0;
}

/*
 * Run Hangman ChatBot Game
 *
 *
 * Returns:
 *   0, if the game has ended successfully
 *   1, if the game failed
 */
int chatbot_do_HangMan(int inc, char* inv[], char* response, int n) {
	if (hangman.welcome == 0) {
		system("cls");
		bot_state = HangManState;
		snprintf(response, n, "\n  _    _          _   _  _____ __  __          _   _ \n");
		snprintf(response + strlen(response), n, " | |  | |   /\\   | \\ | |/ ____|  \\/  |   /\\   | \\ | |\n");
		snprintf(response + strlen(response), n, " | |__| |  /  \\  |  \\| | |  __| \\  / |  /  \\  |  \\| |\n");
		snprintf(response + strlen(response), n, " |  __  | / /\\ \\ | . ` | | |_ | |\\/| | / /\\ \\ | . ` |\n");
		snprintf(response + strlen(response), n, " | |  | |/ ____ \\| |\\  | |__| | |  | |/ ____ \\| |\\  |\n");
		snprintf(response + strlen(response), n, " |_|  |_/_/    \\_\\_| \\_|\\_____|_|  |_/_/    \\_\\_| \\_|\n");
		hangman.guess_count = MAX_GUESSES; //set max guesses to define value at start of file.
		hangman.welcome = 1; //finished with Hangman intro
		//snprintf(response + strlen(response), n, "Player 1, enter a word of no more than %d letters:", max_length);
		snprintf(response + strlen(response), n, "Welcome to Hangman! Would you like to play in:\n(1)Single Player\n(2)MultiPlayer\n\nTo quit the game, Enter \"quit\".");

		return 0;

	}
	else if (hangman.welcome == 1) { // Welcome message printed
		if (hangman.singleormulti != 1) { //User has not selected mode
			if (inv[0][0] == '1') { //Single Player mode
				hangman.singleormulti = 1;
				//snprintf(response, n, "You have chosen to play in SINGLE PLAYER MODE!\n");
				/*Setting the proper names for output*/
				strcpy(hangman.playerName, "Player");
				strcpy(hangman.winnerName, "Chatbot");

				/*Setting mode of hangman*/
				hangman.Mode = inv[0][0];

				/*Read from hangman.txt*/
				char line[10][128];
				FILE* fptr = NULL;
				int i = 0;
				int lines = 0;
				int a = 128;

				fptr = fopen("hangman.txt", "rb"); //open file containing all the possible words for hangman
				if (fptr == NULL) //File not found
				{
					printf("File: hangman.txt not found! \n");
					//go back to default state
					bot_state = Default;
					//reset the progress
					hangman.welcome = 0;
					hangman.guess_count = 0;
					strcpy(hangman.playerName, "");
					strcpy(hangman.winnerName, "");
					hangman.singleormulti = 0;
					hangman.Mode = 0;
					hangman.word_isvalid = 0;
					return 0;
				}
				while (fgets(line[i], a, fptr))
				{
					line[i][strlen(line[i])];
					i++;
				}
				lines = i;

				/*Randomize the word upon each runtime*/
				srand((unsigned)time(0));
				int randomNum = (rand() % lines);	//generate random number
				strncpy(hangman.hangman_word, line[randomNum], (strlen(line[randomNum]) - 2 * sizeof(char))); // Assign random word to hangman word, does not copy null byte at the end
				//printf("%s\n", hangman.hangman_word);

				for (int i = 0; i < strlen(hangman.hangman_word); i++) {//Change word to lowercase chars

					hangman.hangman_word[i] = tolower(hangman.hangman_word[i]);
					hangman.p2gArr[i] = '_'; //initialises array of _ for user to guess
				}
				//printf("REACHED HERE! %d\n", hangman.word_isvalid);

				/*Initialise isvalid to zero so ChatBot random generated word will not be checked*/
				hangman.word_isvalid = 1;
				snprintf(response, n, "So you want to challenge me?\nI have generated a word! Let's Start the game!\nThe Word is: %s \nYou have %d guesses remaining. Enter your next guess:", hangman.p2gArr, hangman.guess_count);
				return 0;
			}
			else if (inv[0][0] == '2') {//Multi Player mode
				snprintf(response, n, "You have chosen to play in MULTI PLAYER MODE!\n");
				hangman.singleormulti = 1;
				/*Setting the proper names for output*/
				strcpy(hangman.playerName, "Player 2");
				strcpy(hangman.winnerName, "Player 1");

				/*Setting mode of hangman*/
				hangman.Mode = inv[0][0];
				/*Initialise isvalid to zero so player 1 input will be checked*/
				hangman.word_isvalid = 0;
				snprintf(response + strlen(response), n, "Player 1, Please enter a word of no more than %d letters:", max_length);
				return 0;
			}
			else if (inv[0][0] != '1' && inv[0][0] != '2') {

				if (strcmp(inv[0], "quit") == 0 || strcmp(inv[0], "Quit") == 0 || strcmp(inv[0], "QUIT") == 0) { //User chooses quit game
					snprintf(response, n, "Quitting Game!\n");
					//go back to default state
					bot_state = Default;
					hangman.welcome = 0;
					return 0;
				}
				else {
					snprintf(response, n, "Invalid Input! Please choose a mode:\n(1)Single Player\n(2)MultiPlayer");
					return 0;
				}
			}
		}
		if (hangman.word_isvalid != 1) { //Player 1 input checking

			if (checkAlpha(inv[0]) == 0) {
				snprintf(response, n, "Invalid Word!\nPlayer 1, Please enter a word of no more than %d letters:", max_length);
				return 0;
			}
			else {// Valid word entered!

				strcpy(hangman.hangman_word, inv[0]); //copies word to hangman.word
				for (int i = 0; i < strlen(hangman.hangman_word); i++) {//Change word to lowercase chars
					hangman.hangman_word[i] = tolower(hangman.hangman_word[i]); //changes all letters in p1guess to lower case
					hangman.p2gArr[i] = '_'; //initialises p2 guesses in array
				}
				hangman.word_isvalid = 1;
				system("CLS"); //clear screen
				snprintf(response, n, "Valid Word! Let's Start the game!\nThe Word is: %s \n%s, you have %d guesses remaining. Enter your next guess:", hangman.p2gArr, hangman.playerName, hangman.guess_count);
				return 0;
			}
		}
		else {
			if (strlen(inv[0]) > 1) { //if p2 enters more than 1 character.
				snprintf(response, n, "Invalid guess!\n%s, please enter your guess 1 character at a time!", hangman.playerName);
				return 0;
			}
			if (hangman.guess_count == 1) { //Player 2 runs out of guesses, Player 1 wins.
				system("cls");
				snprintf(response, n, "\n\t||===== ");
				snprintf(response + strlen(response), n, "\n\t||    | ");
				snprintf(response + strlen(response), n, "\n\t||   %cX/", '\\');
				snprintf(response + strlen(response), n, "\n\t||    | ");
				snprintf(response + strlen(response), n, "\n\t||   / %c", '\\');
				snprintf(response + strlen(response), n, "\n\t||      ");
				snprintf(response + strlen(response), n, "\n\t=========\n");
				snprintf(response + strlen(response), n, "%s has ran out of guesses! %s wins!\nThe word was \'%s\', Better luck next time!", hangman.playerName, hangman.winnerName, hangman.hangman_word);
				bot_state = Default; //exit out of hangman, returns state to defult.
				//reset the progress
				hangman.welcome = 0;
				hangman.guess_count = 0;
				strcpy(hangman.playerName, "");
				strcpy(hangman.winnerName, "");
				hangman.singleormulti = 0;
				hangman.Mode = 0;
				hangman.word_isvalid = 0;
				for (int i = 0; i < max_length; i++) {
					//printf("line %d\n", i);
					hangman.hangman_word[i] = 0;
					hangman.p2gArr[i] = 0;
				}
				return 0;
			}
			snprintf(response, n, "%s, you have %d guesses remaining. Enter your next guess:", hangman.playerName, hangman.guess_count);

			if (isalpha(inv[0][0]) == 0) { //if not alphanumeric, invalid guess
				snprintf(response, n, "Sorry %s, only alphabets allowed! \n%s, you have %d guesses remaining. Enter your next guess:", hangman.playerName, hangman.playerName, hangman.guess_count);
				return 0;
			}
			else { //Player 2 input is valid, now check against the hangman word
				int r = guesscheck(inv[0][0]);
				if (r == 0) { //Check P2 guess
					hangman.guess_count--;

					if (hangman.guess_count == 9) {
						snprintf(response, n, "\n\t        ");
						snprintf(response + strlen(response), n, "\n\t        ");
						snprintf(response + strlen(response), n, "\n\t        ");
						snprintf(response + strlen(response), n, "\n\t        ");
						snprintf(response + strlen(response), n, "\n\t        ");
						snprintf(response + strlen(response), n, "\n\t        \n");
						snprintf(response + strlen(response), n, "\n\t=========\n");
					}
					else if (hangman.guess_count == 8) {
						snprintf(response, n, "\n\t||===== ");
						snprintf(response + strlen(response), n, "\n\t||      ");
						snprintf(response + strlen(response), n, "\n\t||      ");
						snprintf(response + strlen(response), n, "\n\t||      ");
						snprintf(response + strlen(response), n, "\n\t||      ");
						snprintf(response + strlen(response), n, "\n\t||      ");
						snprintf(response + strlen(response), n, "\n\t=========\n");

					}
					else if (hangman.guess_count == 7) {
						snprintf(response, n, "\n\t||===== ");
						snprintf(response + strlen(response), n, "\n\t||    | ");
						snprintf(response + strlen(response), n, "\n\t||      ");
						snprintf(response + strlen(response), n, "\n\t||      ");
						snprintf(response + strlen(response), n, "\n\t||      ");
						snprintf(response + strlen(response), n, "\n\t||      ");
						snprintf(response + strlen(response), n, "\n\t=========\n");

					}
					else if (hangman.guess_count == 6) {
						snprintf(response, n, "\n\t||===== ");
						snprintf(response + strlen(response), n, "\n\t||    | ");
						snprintf(response + strlen(response), n, "\n\t||    O ");
						snprintf(response + strlen(response), n, "\n\t||      ");
						snprintf(response + strlen(response), n, "\n\t||      ");
						snprintf(response + strlen(response), n, "\n\t||      ");
						snprintf(response + strlen(response), n, "\n\t=========\n");
					}
					else if (hangman.guess_count == 5) {
						snprintf(response, n, "\n\t||===== ");
						snprintf(response + strlen(response), n, "\n\t||    | ");
						snprintf(response + strlen(response), n, "\n\t||   %cO ", '\\');
						snprintf(response + strlen(response), n, "\n\t||      ");
						snprintf(response + strlen(response), n, "\n\t||      ");
						snprintf(response + strlen(response), n, "\n\t||      ");
						snprintf(response + strlen(response), n, "\n\t=========\n");
					}
					else if (hangman.guess_count == 4) {
						snprintf(response, n, "\n\t||===== ");
						snprintf(response + strlen(response), n, "\n\t||    | ");
						snprintf(response + strlen(response), n, "\n\t||   %cO/", '\\');
						snprintf(response + strlen(response), n, "\n\t||      ");
						snprintf(response + strlen(response), n, "\n\t||      ");
						snprintf(response + strlen(response), n, "\n\t||      ");
						snprintf(response + strlen(response), n, "\n\t=========\n");
					}
					else if (hangman.guess_count == 3) {
						snprintf(response, n, "\n\t||===== ");
						snprintf(response + strlen(response), n, "\n\t||    | ");
						snprintf(response + strlen(response), n, "\n\t||   %cO/", '\\');
						snprintf(response + strlen(response), n, "\n\t||    | ");
						snprintf(response + strlen(response), n, "\n\t||      ");
						//snprintf(response + strlen(response), n, "\n\t||     %c", '\\');
						snprintf(response + strlen(response), n, "\n\t||      ");
						snprintf(response + strlen(response), n, "\n\t=========\n");
					}
					else if (hangman.guess_count == 2) {
						snprintf(response, n, "\n\t||===== ");
						snprintf(response + strlen(response), n, "\n\t||    | ");
						snprintf(response + strlen(response), n, "\n\t||   %cO/", '\\');
						snprintf(response + strlen(response), n, "\n\t||    | ");
						snprintf(response + strlen(response), n, "\n\t||     %c", '\\');
						snprintf(response + strlen(response), n, "\n\t||      ");
						snprintf(response + strlen(response), n, "\n\t=========\n");
					}
					else {
						snprintf(response, n, "\n\t||===== ");
						snprintf(response + strlen(response), n, "\n\t||    | ");
						snprintf(response + strlen(response), n, "\n\t||   %cO/", '\\');
						snprintf(response + strlen(response), n, "\n\t||    | ");
						snprintf(response + strlen(response), n, "\n\t||   / %c", '\\');
						snprintf(response + strlen(response), n, "\n\t||      ");
						snprintf(response + strlen(response), n, "\n\t=========\n");
						snprintf(response + strlen(response), n, "Sorry %s, wrong letter!\n%s, you have %d guesses remaining. Enter your next guess:\n%s has so far guessed: %s ", hangman.playerName, hangman.playerName, hangman.guess_count, hangman.playerName, hangman.p2gArr);
						return 0;
					}
					snprintf(response + strlen(response), n, "Sorry %s, wrong letter!\n%s, you have %d guesses remaining. Enter your next guess:\n%s has so far guessed: %s ", hangman.playerName, hangman.playerName, hangman.guess_count, hangman.playerName, hangman.p2gArr);
					return 0;
				}
				else if (r == 1) { //Check P2 guess
					if (checkResult() == 1) { //if player 2 guesses the word, he wins
						snprintf(response, n, "Letter found!\n%s has so far guessed: %s \n%s has guess the word! %s wins!\nGoodbye! \n", hangman.playerName, hangman.p2gArr, hangman.playerName, hangman.playerName);
						//go back to default state
						bot_state = Default;
						//reset the progress
						hangman.welcome = 0;
						hangman.guess_count = 0;
						strcpy(hangman.playerName, "");
						strcpy(hangman.winnerName, "");
						hangman.singleormulti = 0;
						hangman.Mode = 0;
						hangman.word_isvalid = 0;
						for (int i = 0; i < max_length; i++) {
							//printf("line %d\n", i);
							hangman.hangman_word[i] = 0;
							hangman.p2gArr[i] = 0;
						}
						return 0;
					}
					else {
						snprintf(response, n, "Letter found!\n%s has so far guessed: %s", hangman.playerName, hangman.p2gArr);
						return 0;
					}
				}
				return 0;
			}
		}

	}
	return 0;
}

/*
 * For Hangman, check p2 input for non-alphabetic characters
 *
 * Input:
 *  input - the word that the player inputs
 *
 * Returns:
 *  1, if the word is valid
 *  0, otherwise
 */
int checkAlpha(char* input) {
	for (int i = 0; i < strlen(input); i++) {
		if (isalpha(input[i]) == 0) {
			printf("Sorry, the word must contain only English letters\n");
			return 0;
		}
	}
	return 1;
}

/*
 * For Hangman, checking character by character
 *
 * Input:
 *  guess - the character that the player guessed
 *
 * Returns:
 *  1, if the character is in the word
 *  0, otherwise
 */
int guesscheck(char guess) {
	int fl = 0;
	for (int i = 0; i < strlen(hangman.hangman_word); i++) {
		if (guess == hangman.hangman_word[i]) {
			hangman.p2gArr[i] = guess;
			fl = 1;
		}
	}
	if (fl == 1) {
		return 1;
	}
	else {
		return 0;
	}
}

/*
 * For Hangman, Check whole string
 *
 * Returns:
 *  1, if the word has been found
 *  0, otherwise
 */
int checkResult() {
	int result = 0;
	for (int i = 0; i < strlen(hangman.hangman_word); i++) {
		if (hangman.p2gArr[i] == hangman.hangman_word[i]) {
			result += 1;
		}
	}
	if (result == strlen(hangman.hangman_word))
		return 1;
	else
		return 0;
}

/*
 * Determine when an intent is play guess a random number game
 *
 *
 * Input:
 *  intent - the intent
 *
 * Returns:
 *  1, if the intent is play random number game
 *  0, otherwise
 */
int chatbot_is_randomNo(const char* intent) {
	if (compare_token(intent, "randomNo") == 0 || compare_token(intent, "randomNum") == 0 || compare_token(intent, "randomNumber") == 0) {
		return 1;
	}
	return 0;
}

/*
 * Run Guess The Number Game
 *
 *
 * Returns:
 *   0, if the game has ended successfully
 *   1, if the game fails to end
 */
int chatbot_do_randomNo(int inc, char* inv[], char* response, int n) {
	if (randomNo.welcome == 0) {
		char randomNoSplash[6][81] = {
			{"______                _                   _   _                 _"},
			{"| ___ \\              | |                 | \\ | |               | |"},
			{"| |_/ /__ _ _ __   __| | ___  _ __ ___   | \\ | |_   _ _ __ ___ | |__   ___ _ __"},
			{"|    // _` | '_ \\ / _` |/ _ \\| '_ ` _ \\  | . ` | | | | '_ ` _ \\| '_ \\ / _ \\ '__|"},
			{"| |\\ \\ (_| | | | | (_| | (_) | | | | | | | |\\  | |_| | | | | | | |_) |  __/ |"},
			{"\\_| \\_\\__,_|_| |_|\\__,_|\\___/|_| |_| |_| \\_| \\_/\\__,_|_| |_| |_|_.__/ \\___|_|"},
		};
		chatbot_textcol_set_chatbot();
		snprintf(response, n, "\n");
		for (int i = 0; i < 6; ++i) {
			snprintf(response + strlen(response), n, " %s\n", randomNoSplash[i]);
		}
		bot_state = RandomNoState;
		snprintf(response + strlen(response), n, "Welcome to random number guessing game, if you guess the right number generated by me, you win!\n");
		randomNo.attempts_left = MAX_GUESSES; //set maximum number of guesses a user is able to guess
		srand((unsigned)time(0));
		randomNo.userWin = 0;
		//chatbot generates a random number
		randomNo.n = (rand() % 999) + 1;
		snprintf(response + strlen(response), n, "I have already chosen a number ranging from 1-999, now is your turn to guess!\n");
		snprintf(response + strlen(response), n, "You have %d attempts left!", randomNo.attempts_left);
		randomNo.welcome = 1; //already welcomed already
		return 0;
	}
	else if (randomNo.welcome == 1) {
		//convert from string to number
		randomNo.userGuess = atoi(inv[0]);
		if (compare_token(inv[0], "quit") == 0) {
			snprintf(response, n, "%s", "You have quit the game, goodbye :-D!");
			bot_state = Default;
			//reset the progress
			randomNo.welcome = 0;
			return 0;
		}
		if (randomNo.userGuess < 1 || randomNo.userGuess >= 1000) {
			snprintf(response, n, "%s", "Sorry, please guess a number from 0 to 999!\n");
			snprintf(response + strlen(response), n, "You have %d attempts left!", randomNo.attempts_left);
			return 0;
		}
		else {
			//if user subsequently ran out of attempts to solve the random number
			if (randomNo.attempts_left == 1) {
				snprintf(response, n, "%s", "Sorry, you ran out of guesses, you lost the game! Try again next time, goodbye :-D!");
				bot_state = Default;
				//reset progress
				randomNo.welcome = 0;
				return 0;
			}
			//if user guesses over
			if (randomNo.userGuess > randomNo.n) {
				randomNo.attempts_left--;
				snprintf(response, n, "Your guess is too high, try again! ");
				snprintf(response + strlen(response), n, "You have %d attempts left!", randomNo.attempts_left);
				return 0;
			}
			//if user guesses under
			else if (randomNo.userGuess < randomNo.n) {
				randomNo.attempts_left--;
				snprintf(response, n, "%s", "Your guess is too low, try again! ");
				snprintf(response + strlen(response), n, "You have %d attempts left!", randomNo.attempts_left);
				return 0;
			}
			//user wins if the user can guess exactly the random number
			else if (randomNo.userGuess == randomNo.n) {
				randomNo.userWin = 1;
			}
		}
		//if user wins, reset progress
		if (randomNo.userWin) {
			snprintf(response, n, "%s", "Congratulations, you have won the random number guessing game, goodbye!");
			//go back to default state
			bot_state = Default;
			//reset the progress
			randomNo.welcome = 0;
			return 0;
		}
	}
	return 0;
}

/*
 * Determine when an intent is display team information/credits
 *
 *
 * Input:
 *  intent - the intent
 *
 * Returns:
 *  1, if the intent is team information
 *  0, otherwise
 */
int chatbot_is_team25Info(const char* intent) {
	if (compare_token(intent, "teamInfo") == 0 || compare_token(intent, "teamCredits") == 0 || compare_token(intent, "team25Credits") == 0 || compare_token(intent, "team25") == 0 || compare_token(intent, "team25Info") == 0) {
		return 1;
	}
	return 0;
}

int chatbot_do_team25Info(int inc, char* inv[], char* response, int n) {
	char splash[18][47] = {
	{"           __   _____  _____  _____          "},
	{"          /  | |  _  ||  _  |/ __  \\         "},
	{"          `| | | |/' || |/' |`' / /'         "},
	{"           | | |  /| ||  /| |  / /           "},
	{"          _| |_\\ |_/ /\\ |_/ /./ /___         "},
	{"          \\___/ \\___/  \\___/ \\_____/         "},
	{" _____  _             _     _             _   "},
	{"/  __ \\| |           | |   | |           | |  "},
	{"| /  \\/| |__    __ _ | |_  | |__    ___  | |_ "},
	{"| |    | '_ \\  / _` || __| | '_ \\  / _ \\ | __|"},
	{"| \\__/\\| | | || (_| || |_  | |_) || (_) || |_ "},
	{" \\____/|_| |_| \\__,_| \\__| |_.__/  \\___/  \\__|"},
	{"   _______                     ___  _____"},
	{"  |__   __|                   |__ \\| ____|"},
	{"     | | ___  __ _ _ __ ___      ) | |__"},
	{"     | |/ _ \\/ _` | '_ ` _ \\    / /|___ \\"},
	{"     | |  __/ (_| | | | | | |  / /_ ___) |"},
	{"     |_|\\___|\\__,_|_| |_| |_| |____|____/"},
	};
	chatbot_textcol_set_chatbot();
	snprintf(response, n, "\n");
	for (int i = 0; i < 18; ++i) {
		snprintf(response + strlen(response), n, "                %s\n", splash[i]);
	}
	snprintf(response + strlen(response), n, "                         %s", "\nThe ICT1002 Chatbot Project is enhanced by the dedicated members of Team 25:\n");
	snprintf(response + strlen(response), n, "                        - %s", "Jared Tan Ting Jie\n");
	snprintf(response + strlen(response), n, "                        - %s", "Leong Jhun Shing John\n");
	snprintf(response + strlen(response), n, "                        - %s", "Jerone Poh Yong Cheng\n");
	snprintf(response + strlen(response), n, "                        - %s", "Lim Chong Jing\n");
	snprintf(response + strlen(response), n, "                        - %s", "Ng De En Gordon\n");
	snprintf(response + strlen(response), n, "                        - %s", "Farin Tahrima Rahman\n");
	snprintf(response + strlen(response), n, "        %s", "Copyright 2019 Singapore Institute of Technology Team 25");
	return 0;
}