/*
 * ICT1002 (C Language) Group Project, AY19 Trimester 1.
 *
 * This file implements the chatbot's knowledge base.
 *
 * knowledge_get() retrieves the response to a question.
 * knowledge_put() inserts a new response to a question.
 * knowledge_read() reads the knowledge base from a file.
 * knowledge_reset() erases all of the knowledge.
 * kowledge_write() saves the knowledge base in a file.
 *
 * You may add helper functions as necessary.
 */
 
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "chat1002.h"

/*This is not part of the skeleton*/
typedef struct knowledge_item KnowledgeItem;
typedef struct knowledge_database KnowledgeDatabase;

struct knowledge_item {
	char intent[MAX_INTENT];
	char entity[MAX_ENTITY];
	char response[MAX_RESPONSE];
	KnowledgeItem* next;
};

struct knowledge_database {
	KnowledgeItem* head;
	KnowledgeItem* tail;
};

KnowledgeDatabase database = { NULL, NULL };

/*
 * Get the response to a question.
 *
 * Input:
 *   intent   - the question word
 *   entity   - the entity
 *   response - a buffer to receive the response
 *   n        - the maximum number of characters to write to the response buffer
 *
 * Returns:
 *   KB_OK, if a response was found for the intent and entity (the response is copied to the response buffer)
 *   KB_NOTFOUND, if no response could be found
 *   KB_INVALID, if 'intent' is not a recognised question word
 */
int knowledge_get(const char *intent, const char *entity, char *response, int n) {

	if(database.head == NULL)
		return KB_NOTFOUND;

	KnowledgeItem* temp = database.head;
	while (temp != NULL) {
		if (compare_token(intent, temp->intent) == 0) {
			if (compare_token(entity, temp->entity) == 0) {
				snprintf(response, n, temp->response);
				return KB_OK;
			}
		}
		temp = temp->next;
	}
	
	return KB_NOTFOUND;
}


/*
 * Insert a new response to a question. If a response already exists for the
 * given intent and entity, it will be overwritten. Otherwise, it will be added
 * to the knowledge base.
 *
 * Input:
 *   intent    - the question word
 *   entity    - the entity
 *   response  - the response for this question and entity
 *
 * Returns:
 *   KB_OK, if successful
 *   KB_NOMEM, if there was a memory allocation failure
 *   KB_INVALID, if the intent is not a valid question word
 */
int knowledge_put(const char* intent, const char* entity, const char* response) {
	
	KnowledgeItem *new_knowledge = malloc(sizeof(KnowledgeItem));

	//Check for memory allocation failure
	if (new_knowledge == NULL) {
		return KB_NOMEM;
	}

	strcpy(new_knowledge->intent, intent);
	strcpy(new_knowledge->entity, entity);
	strcpy(new_knowledge->response, response);
	new_knowledge->next = NULL;

	add_knowledge_to_database(new_knowledge);
	
	return KB_OK;
}


/*
 * Read a knowledge base from a file.
 *
 * Input:
 *   f - the file
 *
 * Returns: the number of entity/response pairs successful read from the file
 */
int knowledge_read(FILE *f) {
	char ch[128];

	int split_pos = 0;
	int found_split = 0;
	char current_intent[10];
	current_intent[0] = '\0';

	while (fgets(ch, sizeof(ch), f) != NULL)
	{
		if (strlen(ch) < 2)
			continue;

		if (ch[0] == '[')
		{
			ch[strlen(ch) - 2] = '\0';
			strcpy(current_intent, ch + 1);
			continue;
		}
		else
		{
			//Continue if invalid line
			if (strlen(current_intent) < 1)
				continue;

			for (int i = 0; i < strlen(ch); i++) {
				if (ch[i] == '=') {
					split_pos = i;
					found_split = 1;
					ch[strlen(ch) - 1] = '\0';
					ch[i] = '\0';
					break;
				}
			}

			if (found_split) {
				KnowledgeItem* new_item = malloc(sizeof(KnowledgeItem));

				//Out of memory
				if (new_item == NULL)
					return 1;

				strcpy(new_item->intent, current_intent);
				strcpy(new_item->entity, ch);
				strcpy(new_item->response, ch + split_pos + 1);
				new_item->next = NULL;
				add_knowledge_to_database(new_item);
			}

			found_split = 0;
		}
	}
	return 0;
}


/*
 * Reset the knowledge base, removing all know entitities from all intents.
 */
void knowledge_reset() {

	if (database.head == NULL)
		return;

	KnowledgeItem* tempptr = NULL;
	
	while (database.head != NULL) { //Loops continuously until database.head==NULL
		/* to be implemented */
		tempptr = database.head;
		database.head = database.head->next;
		free(tempptr);
	}
	database.head = NULL;
	database.tail = NULL;
}


/*
 * Write the knowledge base to a file.
 *
 * Input:
 *   f - the file
 */
void knowledge_write(FILE *f) {
	int what = 0, who = 0, where = 0, which = 0, when = 0, why = 0, how = 0;

	KnowledgeItem* temp = database.head;
	while (temp != NULL) {
		if (compare_token("What", temp->intent) == 0) {
			what++;

			if (what == 1) {
				fprintf(f, "%s", "\n[what]\n");
			}
		}
		else if (compare_token("Who", temp->intent) == 0) {
			who++;
			if (who == 1) {
				fprintf(f, "%s", "\n[who]\n");
			}
		}
		else if (compare_token("When", temp->intent) == 0) {
			when++;
			if (when == 1) {
				fprintf(f, "%s", "\n[when]\n");
			}
		}
		else if (compare_token("Where", temp->intent) == 0) {
			where++;
			if (where == 1) {
				fprintf(f, "%s", "\n[where]\n");
			}
		}
		else if (compare_token("Why", temp->intent) == 0) {
			why++;
			if (why == 1) {
				fprintf(f, "%s", "\n[why]\n");
			}
		}
		else if (compare_token("Which", temp->intent) == 0) {
			which++;
			if (which == 1) {
				fprintf(f, "%s", "\n[which]\n");
			}
		}
		else if (compare_token("How", temp->intent) == 0) {
			which++;
			if (which == 1) {
				fprintf(f, "%s", "\n[how]\n");
			}
		}
		else {}
		fprintf(f, "%s%s", temp->entity, "=");
		fprintf(f, "%s%s", temp->response, "\n");
		temp = temp->next;
	}

}

int add_knowledge_to_database(void* item) {
	KnowledgeItem* new_item = (KnowledgeItem*)item;

	//Check if the database is empty
	if (database.head == NULL) {
		database.head = new_item;
		database.tail = new_item;
		return 0;
	}

	//This should never be null if head is not null! This is only here for error prevention
	if (database.tail == NULL) {
		return 1;
	}

	int found_intent = 0;
	KnowledgeItem* current = database.head;
	KnowledgeItem* prev = NULL;
	while (current != NULL) {
		if (compare_token(current->intent, new_item->intent) == 0) {
			found_intent = 1;
			if (compare_token(current->entity, new_item->entity) == 0) {
				strcpy(current->response, new_item->response);
				free(new_item);
				return 0;
			}
			else {
				prev = current;
				current = current->next;
				continue;
			}
		}
		else if (found_intent) {
			new_item->next = prev->next;
			prev->next = new_item;
			if (new_item->next == NULL)
				database.tail = new_item;
			return 0;
		}
		prev = current;
		current = current->next;
	}

	database.tail->next = new_item;
	database.tail = new_item;

	return 0;
}