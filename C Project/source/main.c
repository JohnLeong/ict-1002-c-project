/*
 * ICT1002 (C Language) Group Project, AY19 Trimester 1.
 *
 * This file implements the main loop, including dividing input into words.
 *
 * You should not need to modify this file. You may invoke its functions if you like, however.
 */

#include <ctype.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "chat1002.h"
#include <windows.h>
#include <mmsystem.h>
#pragma comment (lib, "winmm.lib")

void print_splash_screen();
void print_end_screen();

/* word delimiters */
const char *delimiters = " ?\t\n";

/*
 * Main loop.
 */
int main(int argc, char *argv[]) {

	char input[MAX_INPUT];      /* buffer for holding the user input */
	int inc;                    /* the number of words in the user input */
	char *inv[MAX_INPUT];       /* pointers to the beginning of each word of input */
	char output[MAX_RESPONSE];  /* the chatbot's output */
	int len;                    /* length of a word */
	int done = 0;               /* set to 1 to end the main loop */

	/* initialise the chatbot */
	inv[0] = "reset";
	inv[1] = NULL;
	chatbot_do_reset(1, inv, output, MAX_RESPONSE, 0);

	print_splash_screen();

	/* print a welcome message */
	chatbot_textcol_set_chatbot();
	printf("%s: Hello, I'm %s.\n", chatbot_botname(), chatbot_botname());
	chatbot_textcol_set_default();

	/* main command loop */
	do {

		do {
			/* read the line */
			chatbot_textcol_set_user();
			printf("%s: ", chatbot_username());
			fgets(input, MAX_INPUT, stdin);
			if (!strchr(input, '\n'))
				while (fgetc(stdin) != '\n');
			chatbot_textcol_set_default();

			/* split it into words */
			inc = 0;
			inv[inc] = strtok(input, delimiters);
			while (inv[inc] != NULL) {

				/* remove trailing punctuation */
				len = (int)strlen(inv[inc]);
				while (len > 0 && ispunct(inv[inc][len - 1])) {
					inv[inc][len - 1] = '\0';
					len--;
				}

				/* go to the next word */
				inc++;
				inv[inc] = strtok(NULL, delimiters);
			}
		} while (inc < 1);

		/* invoke the chatbot */
		done = chatbot_main(inc, inv, output, MAX_RESPONSE);

		chatbot_textcol_set_chatbot();
		printf("\r%s: %s\n", chatbot_botname(), output);
		PlaySound("sound\\ding.wav", NULL, SND_ASYNC | SND_FILENAME);
		chatbot_textcol_set_default();
	} while (!done);

	print_end_screen();
	return 0;
}


/*
 * Utility function for comparing string case-insensitively.
 *
 * Input:
 *   token1 - the first token
 *   token2 - the second token
 *
 * Returns:
 *   as strcmp()
 */
int compare_token(const char *token1, const char *token2) {

	int i = 0;
	while (token1[i] != '\0' && token2[i] != '\0') {
		if (toupper(token1[i]) < toupper(token2[i]))
			return -1;
		else if (toupper(token1[i]) > toupper(token2[i]))
			return 1;
		i++;
	}

	if (token1[i] == '\0' && token2[i] == '\0')
		return 0;
	else if (token1[i] == '\0')
		return -1;
	else
		return 1;

}


/*
 * Prompt the user.
 *
 * Input:
 *   buf    - a buffer into which to store the answer
 *   n      - the maximum number of characters to write to the buffer
 *   format - format string, as printf
 *   ...    - as printf
 */
void prompt_user(char *buf, int n, const char *format, ...) {

	/* print the prompt */
	va_list args;
	va_start(args, format);
	printf("%s: ", chatbot_botname());
	vprintf(format, args);
	printf(" ");
	va_end(args);
	printf("\n\r%s: ", chatbot_username());

	/* get the response from the user */
	fgets(buf, n, stdin);
	char *nl = strchr(buf, '\n');
	if (nl != NULL)
		*nl = '\0';
}

/*
 * Prints the splash screen ascii art
 */
void print_splash_screen() {
	char splash[18][47] = {
	{"           __   _____  _____  _____          "},
	{"          /  | |  _  ||  _  |/ __  \\         "},
	{"          `| | | |/' || |/' |`' / /'         "},
	{"           | | |  /| ||  /| |  / /           "},
	{"          _| |_\\ |_/ /\\ |_/ /./ /___         "},
	{"          \\___/ \\___/  \\___/ \\_____/         "},
	{" _____  _             _     _             _   "},
	{"/  __ \\| |           | |   | |           | |  "},
	{"| /  \\/| |__    __ _ | |_  | |__    ___  | |_ "},
	{"| |    | '_ \\  / _` || __| | '_ \\  / _ \\ | __|"},
	{"| \\__/\\| | | || (_| || |_  | |_) || (_) || |_ "},
	{" \\____/|_| |_| \\__,_| \\__| |_.__/  \\___/  \\__|"},
	{"   _______                     ___  _____"},
	{"  |__   __|                   |__ \\| ____|"},
	{"     | | ___  __ _ _ __ ___      ) | |__"},
	{"     | |/ _ \\/ _` | '_ ` _ \\    / /|___ \\"},
	{"     | |  __/ (_| | | | | | |  / /_ ___) |"},
	{"     |_|\\___|\\__,_|_| |_| |_| |____|____/"},
	};

	chatbot_textcol_set_chatbot();
	printf("\n\n\n");
	for (int i = 0; i < 18; ++i) {
		printf("                                   %s\n", splash[i]);
	}
	chatbot_textcol_set_user();
	printf("\n\n\n                                              Press any key to continue");
	chatbot_textcol_set_default();
	getch();
	PlaySound("sound\\start.wav", NULL, SND_ASYNC | SND_FILENAME);
	system("cls");
}

/*
 * Prints the end screen ascii art
 */
void print_end_screen() {
	char end[][47] = {
	{"   _____                 _ _                _ "},
	{"  / ____|               | | |              | |"},
	{" | |  __  ___   ___   __| | |__  _   _  ___| |"},
	{" | | |_ |/ _ \\ / _ \\ / _` | '_ \\| | | |/ _ \\ |"},
	{" | |__| | (_) | (_) | (_| | |_) | |_| |  __/_|"},
	{"  \\_____|\\___/ \\___/ \\__,_|_.__/ \\__, |\\___(_)"},
	{"                                  __/ |       "},
	{"                                 |___/        "},
	};

	system("cls");
	chatbot_textcol_set_chatbot();
	printf("\n\n\n\n\n\n\n\n");
	for (int i = 0; i < 8; ++i) {
		printf("                                   %s\n", end[i]);
	}
	getch();
}